/**
 * @File Name          : sendQuoteDetails.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 9/18/2019, 2:39:28 PM
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    9/12/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public class sendQuoteDetails {

    
    Opportunity theOpp;

    public sendQuoteDetails(ApexPages.StandardController stdController) {
        // get the current Account
        theOpp = (Opportunity)stdController.getRecord();
    }

    public PageReference doMyApexLogic() {

        makePostCallout(theOpp.Id);
        //whatever logic you want

        //</demo logic>
        return new PageReference('Api post has been made');
    }

    public static HttpResponse makePostCallout(String opportunityId) {
                
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        //request.setEndpoint('https://murmuring-hamlet-53234.herokuapp.com/api/genrateQuote');
        request.setEndpoint('callout:GenerateQuoteService/api/genrateQuote');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        request.setBody('{"opportunityId": "'+opportunityId+'"}');
        // the token access or API key should be put into a custom settings in SF org
        //request.setHeader( 'Authorization', ']NLZA(SX5PFPj!pqme!+7IIN,0sPe.J0`fZ-2L-hi{k1Hacu,bZ_]C#tk#l:UPBV.w=Da;O4lAY:&k#l:UPBV.w=Da;O4lAY:&k#l:UPBV.w=Da;O4lAY:&k#l:UPBV.w=Da;O4lAY:&');
        request.setHeader( 'Authorization', '{!$Credential.Password}');
        HttpResponse response = http.send(request);      
        
        System.debug(response);
        System.debug(response.getBody());
        
        // hfdhfsdjklvndjkvd
        return response;
    }

}