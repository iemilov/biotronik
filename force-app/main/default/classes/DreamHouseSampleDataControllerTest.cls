/**
 * @File Name          : DreamHouseSampleDataControllerTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 10/2/2019, 2:23:23 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/2/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest
private class DreamHouseSampleDataControllerTest {

    @isTest
    static void test_deleteAll() {

        Property__c property = new Property__c();
        Broker__c broker = new Broker__c();

        insert property;
        insert broker;

        System.assertEquals( 1, [ SELECT count() FROM Property__c ] );
        System.assertEquals( 1, [ SELECT count() FROM Broker__c ] );

        Test.startTest();

        DreamHouseSampleDataController.deleteAll();

        Test.stopTest();

        System.assertEquals( 0, [ SELECT count() FROM Property__c ] );
        System.assertEquals( 0, [ SELECT count() FROM Broker__c ] );

    }

}