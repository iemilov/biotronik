/**
 * @File Name          : popup.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 9/18/2019, 2:48:01 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    9/18/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public class popup {

    public popup(ApexPages.StandardController stdController) {
        // get the current Account
    }

    public boolean displayPopup {get; set;}

    public void closePopup() {
        displayPopup = false;
    }

    public void showPopup() {
        displayPopup = true;
    }
}