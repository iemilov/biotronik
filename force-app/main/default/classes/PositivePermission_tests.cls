/**
 * @File Name          : PositivePermission_tests.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 4/20/2020, 5:47:59 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/20/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest
public with sharing class PositivePermission_tests {

    @TestSetup
    static void testSetup(){
      Account a = TestFactory.getAccount('No view For You!', true);
      Private_Object__c po = new Private_Object__c(account__c = a.id, notes__c = 'foo');
      insert po;
    }
    
    @isTest static void PermissionSetTest_Positive() {
        User u = TestFactory.generateUser('Custom User');
        System.runAs(u){
          Private_Object__c[] pos;
          Test.startTest();
          pos = [SELECT Id, Account__c, notes__c FROM Private_Object__c];
          Test.stopTest();
          system.assertEquals(pos.size(),1);
        }
      }
}
