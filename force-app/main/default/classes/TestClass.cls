/**
 * @File Name          : TestClass.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 1/9/2020, 9:24:55 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/9/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public with sharing class TestClass {

private final Decimal discountedRate = 0.85;
  private Decimal price;
  private String name = 'Ivan';

  public TestClass(String name, Decimal price) {
    this.price = price;
    this.name = name;
  }

  public Decimal getPrice() {
    return price * 5;
  }

  public Decimal getDiscount() {
    return price * discountedRate;
  } 

}

/*

TestClass obj = new TestClass('JJJ', 123);
System.debug(obj);

*/