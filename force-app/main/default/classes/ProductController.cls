/**
 * @File Name          : ProductController.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 1/9/2020, 9:28:45 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/9/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public with sharing class ProductController {
  // This list can hold any product that implements the Product interface
  public List<Product> allProducts{get;set;}
 
  public ProductController() {
    allProducts = new List<Product>();
 
    // Instantiate some fruit and financial products
    FruitProduct banana = new FruitProduct('Banana', 2);

    FruitProduct dragonFruit = new FruitProduct('Dragon Fruit', 2.50); 
 
    FinancialProduct mortgage = new FinancialProduct('Mortgage', 100);
    FinancialProduct spreadBet = new FinancialProduct('Spread Bet', 5);
 
    // And add them to the list
    allProducts.add(banana);
    allProducts.add(dragonFruit);
    allProducts.add(mortgage);
    allProducts.add(spreadBet);
  }
}