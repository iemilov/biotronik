/**
 * @File Name          : myDataGenerationTests.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 4/20/2020, 4:43:17 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/20/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest
public with sharing class myDataGenerationTests {


      @TestSetup
      static void loadTestDataFromStaticResource(){
        List<sObject> accounts = Test.loadData(Account.SObjectType, 'Mock_Data');
      }
      @isTest static void testLoadAccountsFromStaticResource() {
        List<Account> accts = [SELECT ID FROM Account];
        system.assert(accts.size() == 15, 'expected 15 accounts');
      }
}