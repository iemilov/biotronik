/**
 * @File Name          : Product.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 1/9/2020, 8:45:06 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/9/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/

public interface Product {
  
  /** 
    Any class that implements this interface must
    have methods with the following signatures
  **/
 
  Decimal getDiscount();
  Decimal getPrice();
  String getName();
 
}