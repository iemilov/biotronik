/**
 * @File Name          : DreamHouseSampleDataController.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 11/29/2019, 1:44:20 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/2/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
global with sharing class DreamHouseSampleDataController {

    @RemoteAction
    // this is a demo with the new image
    global static void deleteAll() {
        DELETE [SELECT ID FROM property__c];    
        DELETE [SELECT ID FROM broker__c];
    }    
}